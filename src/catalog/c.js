import {
  d1,
  d2,
  d12,
  d3,
  d4,
  d5,
  d33,
  d34,
  d35,
  d36,
  d37,
  d133,
  d134,
  d135,
  d233,
  d433,
  d434,
  d435,
  d412,
  d512
} from './d'
////////////////////////////////////非特殊命名系列//////////////////////////////

//成品和功能件级3
export function c0() {
  return [
    {name: '机械开关类', code: '-1', children: d1()},
    {name: '电子开关类', code: '-2', children: d2()},
    {name: '强电插座类', code: '-3', children: d3()},
    {name: '弱电插座类', code: '-4', children: d4()},
    {name: '其他功能类', code: '-5', children: d5()}
  ]
}

//面板联
export function c1() {
  return [
    {name: '单联', id: 101, code: '1', children: d12()},
    {name: '2联', id: 102, code: '2', children: d12()},
    {name: '3联', id: 103, code: '3', children: d12()},
    {name: '4联', id: 104, code: '4', children: d12()},
    {name: '5联', id: 105, code: '5', children: d12()}
  ]
}


//材质
export function c2() {
  return [
    {name: '塑胶', id: 101, code: '-1', children: d33()},
    {name: '五金', id: 102, code: '-2', children: d34()},
    {name: '玻璃', id: 103, code: '-3', children: d35()},
    {name: '氧化体磁铁', id: 104, code: '-4', children: d36()},
    {name: '木材', id: 105, code: '-5', children: d37()}
  ]
}

///////////////////////////////////特殊命名系列///////////////////////////////////
//表8
export function c3() {
  return [
    {name: '塑胶', id: 101, code: '-1', children: d133()},
    {name: '五金', id: 102, code: '-2', children: d134()},
    {name: '玻璃', id: 103, code: '-3', children: d135()},
    {name: '其他', id: 104, code: '-3', children: d135()}
  ]
}

//表9 元器件名称
export function c4() {
  return [
    {name: '元器件', id: 101, code: '-1', children: d233()}
  ]
}

//表10 工装夹具类
export function c5() {
  return [
    {name: '检测夹具', id: 101, code: '1', children: d433()},
    {name: '生产夹具', id: 102, code: '2', children: d434()},
    {name: '销售夹具', id: 103, code: '3', children: d435()}
  ]
}

//表12 辅料耗材类型
export function c12() {
  return [
    {name: '辅料耗材1', id: 101, code: '-1', children: d412()}
  ]
}

//表14 包材名称
export function c14() {
  return [
    {name: '包材名称1', id: 101, code: '-1', children: d512()}
  ]
}
