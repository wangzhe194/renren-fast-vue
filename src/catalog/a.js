import {b0, b1} from './b'

export function a0() {
  return [
    {name: '特殊命名系列', id: 0, code: 0, children: b1()},
    {name: '延伸产品系列', id: 1, code: 1, children: b0()},
    {name: '新意式系列', id: 2, code: 2, children: b0()},
    {name: '86型外贸系列', id: 3, code: 3, children: b0()},
    {name: '预留', id: 4, code: 4, children: b0()},
    {name: '南非系列', id: 5, code: 5, children: b0()},
    {name: '新英式系列', id: 6, code: 6, children: b0()},
    {name: '欧式系列', id: 7, code: 7, children: b0()},
    {name: '新美式系列', id: 8, code: 8, children: b0()},
    {name: '澳式、巴西系列', id: 9, code: 9, children: b0()}
  ]
}

