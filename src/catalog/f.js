//注塑或冲压件素材表面颜色
export function f1() {
  return [
    {name: '白色', id: 101, code: '01'},
    {name: '黑色', id: 102, code: '02'},
    {name: '金色', id: 103, code: '03'},
    {name: '银色', id: 104, code: '04'},
    {name: '灰色', id: 105, code: '05'},
    {name: '黄铜色', id: 106, code: '06'},
    {name: '粉红色', id: 106, code: '07'},
    {name: '墨绿色', id: 106, code: '08'},
    {name: '黛蓝色', id: 106, code: '09'},
    {name: '透明色', id: 106, code: '00'}
  ]
}

//表面处理及颜色
export function f2() {
  return [
    {name: '单白色', id: 111, code: '11'},
    {name: '单黑色', id: 12, code: '12'},
    {name: '单金色', id: 113, code: '13'},
    {name: '单银色', id: 113, code: '14'},
    {name: '单灰色', id: 114, code: '15'},
    {name: '预留', id: 115, code: '16'},
    {name: '单粉红色', id: 116, code: '17'},
    {name: '单墨绿色', id: 116, code: '18'},
    {name: '单黛蓝色', id: 116, code: '19'}
  ]
}

//表面处理及颜色2
export function f3() {
  return [
    {name: '铜丝拉纹', id: 11, code: '97'},
    {name: '木纹+光油', id: 12, code: '99'},
    {name: '镀黄铜', id: 13, code: '98'},
    {name: '镀镍', id: 14, code: '96'},
    {name: '镀白锌', id: 15, code: '95'},
    {name: '镀彩锌', id: 16, code: '94'},
    {name: '镀铬', id: 16, code: '93'},
    {name: '氧化白', id: 16, code: '92'},
    {name: '氧化黑', id: 16, code: '91'},
    {name: '喷砂', id: 16, code: '90'}
  ]
}

/////////////////////////////////////特殊编码

//材质类别 ，表11
export function f11() {
  return [
    {name: '铝制夹具', id: 101, code: '01'},
    {name: '电木夹具', id: 102, code: '02'},
    {name: '树脂夹具', id: 103, code: '03'},
    {name: '钢制夹具', id: 104, code: '04'}
  ]
}

export function f13() {
  return [
    {name: '产品防护', id: 101, code: '01'},
    {name: '设备用品', id: 102, code: '02'},
    {name: '清洁用品', id: 103, code: '03'},
    {name: '劳保用品', id: 104, code: '04'},
    {name: '产品直接用料', id: 105, code: '05'}
  ]
}
