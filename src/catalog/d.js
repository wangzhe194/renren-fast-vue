//面板孔数
export function d12() {
  return [
    {name: '0孔', id: 101, code: '0'},
    {name: '单孔', id: 101, code: '1'},
    {name: '2孔', id: 102, code: '2'},
    {name: '3孔', id: 103, code: '3'},
    {name: '4孔', id: 104, code: '4'},
    {name: '5孔', id: 105, code: '5'}
  ]
}

//机械开关类
export function d1() {
  return [
    {name: '琴键开关', id: 101, code: '01'},
    {name: '复位开关', id: 102, code: '02'},
    {name: '中途开关', id: 103, code: '03'},
    {name: '窗帘开关', id: 104, code: '04'},
    {name: '调速开关', id: 105, code: '05'},
    {name: '音量开关', id: 106, code: '06'},
    {name: '调光开关', id: 106, code: '07'}
  ]
}

//电子开关类
export function d2() {
  return [
    {name: '复位遥控开关', id: 111, code: '10'},
    {name: '声光控延时开关', id: 12, code: '11'},
    {name: '调光开关', id: 113, code: '12'},
    {name: '调光遥控开关', id: 113, code: '13'},
    {name: '窗帘开关', id: 114, code: '14'},
    {name: '遥控窗帘开关', id: 115, code: '15'},
    {name: '调光延时开关', id: 116, code: '16'},
    {name: '触摸延时开关', id: 111, code: '17'},
    {name: '音量开关', id: 12, code: '18'},
    {name: '智能开关', id: 113, code: '19'},
    {name: '触摸开关', id: 113, code: '20'},
    {name: '遥控触摸开关', id: 114, code: '21'},
    {name: '复位开关', id: 115, code: '22'},
    {name: '调速开关', id: 116, code: '23'},
    {name: 'ZB调光开关', id: 111, code: '24'},
    {name: 'ZB窗帘开关', id: 12, code: '25'},
    {name: 'ZB开关', id: 113, code: '26'},
    {name: 'ZB网关', id: 113, code: '27'},
    {name: '人体感应开关', id: 114, code: '28'},
    {name: 'ZB遥控调光开关', id: 115, code: '29'},
    {name: 'ZB遥控窗帘开关', id: 116, code: '30'},
    {name: '双极性开关', id: 115, code: '31'},
    {name: '双极性遥控开关', id: 116, code: '32'}
  ]
}

//强电插座类
export function d3() {
  return [
    {name: '二孔插', id: 11, code: '40'},
    {name: '双二孔插', id: 12, code: '41'},
    {name: '三孔插', id: 13, code: '42'},
    {name: '双三孔插', id: 14, code: '43'},
    {name: '一开三孔插', id: 15, code: '44'},
    {name: '五孔插', id: 16, code: '45'},
    {name: '一开五孔插', id: 16, code: '46'},
    {name: '七孔插', id: 16, code: '47'},
    {name: '一开七孔插', id: 16, code: '48'},
    {name: '触摸双三孔插', id: 16, code: '49'},
    {name: '触摸1路三孔插', id: 16, code: '50'},
    {name: '触摸遥控双三孔插', id: 16, code: '51'},
    {name: '触摸遥控一路三孔插', id: 16, code: '52'},
    {name: 'ZB触摸双三孔插', id: 16, code: '53'},
    {name: '一开两孔插', id: 16, code: '54'}
  ]
}

//弱电插座类
export function d4() {
  return [
    {name: '电话插', id: 11, code: '70'},
    {name: '电脑插', id: 12, code: '71'},
    {name: '卫星插', id: 13, code: '72'},
    {name: '高清插', id: 14, code: '73'},
    {name: '音频插', id: 15, code: '74'},
    {name: '音响插', id: 16, code: '75'},
    {name: 'USB插', id: 16, code: '76'},
    {name: 'TYPE-C插', id: 16, code: '77'},
    {name: '电视插', id: 16, code: '78'},
    {name: '话筒类插', id: 16, code: '79'},
  ]
}

//其他功能类
export function d5() {
  return [
    {name: '防水盖', id: 11, code: '99'},
    {name: '空白板', id: 12, code: '98'},
    {name: '暗盒', id: 13, code: '97'},
    {name: '温控器', id: 14, code: '96'},
    {name: '遥控器', id: 15, code: '95'},
    {name: '门铃', id: 16, code: '94'},
    {name: '适配器', id: 16, code: '93'},
    {name: 'ZB温控器', id: 16, code: '92'}
  ]
}


//塑胶
export function d33() {
  return [
    {name: '塑胶上盖', id: 11, code: '01'},
    {name: '塑胶底壳', id: 12, code: '02'},
    {name: '压板', id: 13, code: '03'},
    {name: '安全门', id: 14, code: '04'},
    {name: '拿子', id: 15, code: '05'},
    {name: '打子', id: 16, code: '06'},
    {name: '基座', id: 16, code: '07'},
    {name: '装饰圈', id: 16, code: '08'},
    {name: '支架', id: 16, code: '09'},
    {name: '固定架', id: 16, code: '10'},
    {name: '塑胶片', id: 16, code: '11'},
    {name: '拔片盖', id: 12, code: '12'},
    {name: '按键', id: 13, code: '13'},
    {name: '安装板', id: 14, code: '14'},
    {name: '透镜', id: 15, code: '15'},
    {name: '导光圈', id: 16, code: '16'},
    {name: '推杆', id: 16, code: '17'},
    {name: '线圈罩', id: 16, code: '18'},
    {name: '模块', id: 16, code: '19'}
  ]
}

//五金
export function d34() {
  return [
    {name: '铁板', id: 11, code: '41'},
    {name: '压线板（垫片）', id: 12, code: '42'},
    {name: '铜翘板', id: 13, code: '43'},
    {name: '零线端子', id: 14, code: '44'},
    {name: '火线端子', id: 15, code: '45'},
    {name: '接地端子', id: 16, code: '46'},
    {name: '零线弹片', id: 16, code: '47'},
    {name: '火线弹片', id: 16, code: '48'},
    {name: '地线弹片', id: 16, code: '49'},
    {name: '不锈钢装饰板', id: 16, code: '50'},
    {name: '铝支架', id: 11, code: '51'},
    {name: '铜面板', id: 12, code: '52'},
    {name: '动簧片', id: 13, code: '53'},
    {name: '静簧片', id: 14, code: '54'},
    {name: '闭磁环', id: 15, code: '55'},
    {name: '轭铁', id: 16, code: '56'},
    {name: '衔铁', id: 16, code: '57'},
    {name: '插针', id: 16, code: '58'},
    {name: '散热片', id: 16, code: '59'},
    {name: '接插头', id: 16, code: '60'}
  ]
}

//玻璃
export function d35() {
  return [
    {name: '插孔面板', id: 11, code: '81'},
    {name: '圆显示面板', id: 12, code: '82'},
    {name: '装饰面板玻璃原片', id: 13, code: '83'}
  ]
}

//氧化体磁铁
export function d36() {
  return [
    {name: '磁铁条', id: 11, code: '99'}
  ]
}

//木材
export function d37() {
  return [
    {name: '原木面板', id: 11, code: '98'}
  ]
}

////////////////////////////////////特殊命名系列//////////////////////////////

//塑胶
export function d133() {
  return [
    {name: '米粒', id: 11, code: '01'},
    {name: '端子固定端', id: 12, code: '02'},
    {name: '导光片', id: 13, code: '03'}
  ]
}

//五金
export function d134() {
  return [
    {name: '铁板', id: 11, code: '41'},
    {name: '垫片', id: 12, code: '42'},
    {name: '圆柱螺钉', id: 13, code: '43'},
    {name: '螺母', id: 14, code: '44'},
    {name: '弹簧', id: 15, code: '45'},
    {name: '铜柱', id: 16, code: '46'}
  ]
}

//其他
export function d135() {
  return [
    {name: '其他', id: 11, code: '81'}
  ]
}

//表9 元器件名称
export function d233() {
  return [
    {name: '电路板', id: 11, code: '01'},
    {name: '集成电路', id: 12, code: '02'},
    {name: '二极管', id: 13, code: '03'},
    {name: '三极管', id: 14, code: '04'},
    {name: '晶闸管', id: 15, code: '05'},
    {name: '电容', id: 16, code: '06'},
    {name: '电感', id: 16, code: '07'},
    {name: '晶体振荡管', id: 16, code: '08'},
    {name: '电位器', id: 16, code: '09'},
    {name: '继电器', id: 16, code: '10'},
    {name: '变压器', id: 16, code: '11'},
    {name: '连接器', id: 12, code: '12'},
    {name: '电阻', id: 13, code: '13'},
    {name: '磁珠', id: 14, code: '14'},
    {name: '天线', id: 15, code: '15'},
    {name: '蜂鸣器', id: 16, code: '16'},
    {name: '电池', id: 16, code: '17'},
    {name: '排针', id: 16, code: '18'},
    {name: '排母', id: 16, code: '19'},
    {name: '场效应管', id: 16, code: '20'},
    {name: '印刷电路板', id: 16, code: '21'},
    {name: '电子线材', id: 16, code: '22'},
    {name: '扬声器', id: 16, code: '23'},
    {name: '保险丝', id: 16, code: '24'},
    {name: '数码管', id: 16, code: '25'}
  ]
}

//表10
export function d433() {
  return [
    {name: '性能测试夹具', id: 11, code: '1'},
    {name: '安规测试夹具', id: 12, code: '2'}
  ]
}

//表10
export function d434() {
  return [
    {name: '手工焊接夹具', id: 11, code: '1'},
    {name: '机器焊接夹具', id: 12, code: '2'},
    {name: '打螺丝夹具', id: 11, code: '3'},
    {name: '玻璃粘合夹具', id: 12, code: '4'},
    {name: '镭雕定位夹具', id: 11, code: '5'},
    {name: '贴片夹具', id: 12, code: '6'},
    {name: '装配夹具', id: 12, code: '7'}
  ]
}

//表10
export function d435() {
  return [
    {name: '展示夹具', id: 11, code: '1'}
  ]
}

//表 12 耗材材质
export function d412() {
  return [
    {name: '3M胶条（双面胶）', id: 11, code: '01'},
    {name: '胶水', id: 12, code: '02'},
    {name: '锡膏锡线锡棒', id: 13, code: '03'},
    {name: '助焊剂', id: 14, code: '04'},
    {name: '开油水', id: 15, code: '05'},
    {name: '酒精', id: 16, code: '06'},
    {name: '保护膜', id: 16, code: '07'},
    {name: '碎布', id: 16, code: '08'},
    {name: '口罩', id: 16, code: '09'},
    {name: '洗板水', id: 16, code: '10'},
    {name: '丝印油墨', id: 16, code: '11'},
    {name: '清洁去污剂', id: 12, code: '12'},
    {name: '热熔胶', id: 13, code: '13'},
    {name: '封口胶带', id: 14, code: '14'},
    {name: '手套', id: 15, code: '15'},
    {name: '脚套', id: 16, code: '16'},
    {name: '网版1', id: 16, code: '17'},
    {name: '辊轮刷', id: 16, code: '18'}
  ]
}

//表14 包材名称
export function d512() {
  return [
    {name: '外箱（纸）', id: 11, code: '01'},
    {name: '内盒（纸）', id: 12, code: '02'},
    {name: '内衬', id: 13, code: '03'},
    {name: '隔板纸板', id: 14, code: '04'},
    {name: '托盘', id: 15, code: '05'},
    {name: '干燥剂', id: 16, code: '06'},
    {name: '说明书', id: 16, code: '07'},
    {name: '合格证', id: 16, code: '08'},
    {name: '打包带', id: 16, code: '09'},
    {name: '封胶带', id: 16, code: '10'},
    {name: '缓冲泡棉', id: 16, code: '11'},
    {name: '缓冲海绵', id: 12, code: '12'},
    {name: '气泡袋', id: 13, code: '13'},
    {name: '气泡袋2', id: 14, code: '14'},
    {name: 'PE袋', id: 15, code: '15'},
    {name: 'PP袋', id: 16, code: '16'},
    {name: '泡棉袋', id: 16, code: '17'},
    {name: '标签纸', id: 16, code: '18'},
    {name: '封口袋', id: 16, code: '19'}
  ]
}
