import {c0, c1, c2, c3, c4, c5, c12, c14} from './c'
import {e1, e11, e13} from './e'

/*
非特殊命名系列
 */
export function b0() {
  return [
    {name: '成品', id: 10, code: '0', children1: c0(), children3: [], children2: e1()},
    {name: '面板组件', id: 11, code: '1', children1: c1(), children3: [], children2: e1()},
    {name: '功能件组件', id: 12, code: '2', children1: c0(), children3: [], children2: e1()},
    {name: '电子组件', id: 13, code: '3', children1: c0(), children3: [], children2: []},
    {name: '零件（结构件）', id: 14, code: '4', children1: c2(), children3: [], children2: e1()},
    {name: '丝印玻璃（未粘合）', id: 15, code: '5', children1: c1(), children3: [], children2: e1()},
    {name: '软件包', id: 16, code: '6', children1: c0(), children3: [], children2: []},
    {name: '非通用件模具', id: 17, code: '7', children1: c2(), children3: [], children2: []},
    {name: '底座（铁板与功能件组合）', id: 18, code: '8', children1: c0(), children3: [], children2: e1()}
    //{name: '外购件', id: 19, code: '9', children1: },
  ]
}

/*
特殊命名系列
 */
export function b1() {
  return [
    //{name: '预留', id: 10, code: '0', children1: c0(), children3: [] ,children2:e1()},
    {name: '通用结构件类', id: 11, code: '1', children1: c3(), children3: [], children2: e1()},
    {name: '电子元器件类', id: 12, code: '2', children1: c4(), children3: [], children2: []},
    {name: '工装夹具类', id: 13, code: '3', children1: c5(), children3: [], children2: e11()},
    {name: '辅料/耗材类', id: 14, code: '4', children1: c12(), children3: [], children2: e13()},
    {name: '包材类', id: 15, code: '5', children1: c14(), children3: [], children2: []},
    //{name: '资产类', id: 16, code: '6',children1: c0(), children3: [] ,children2: [] },
    {name: '通用零件模具类', id: 17, code: '7', children1: c3(), children3: [], children2: []}
  ]
}


