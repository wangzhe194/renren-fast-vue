import {f1, f2, f3, f11, f13} from './f'

//颜色与材质分类
export function e1() {
  return [
    {name: '注塑或冲压件素材表面颜色', id: 101, code: '-1', children: f1()},
    {name: '表面处理及颜色', id: 102, code: '-2', children: f2()},
    {name: '表面处理及颜色2', id: 103, code: '-3', children: f3()}
  ]
}

///////////////特殊编码///////////////////////
//夹具材质 表11
export function e11() {
  return [
    {name: '夹具材质类别', id: 101, code: '-1', children: f11()}
  ]
}

//耗材属性类别 表13
export function e13() {
  return [
    {name: '耗材属性类别', id: 101, code: '-1', children: f13()}
  ]
}
