export default {
  namespaced: true,
  state: {
    userInfo:{},
    dingConfig:{},
    unPermDingIds:'',
    leaders:'',
    lastLoginTime: null
  },
  mutations: {
    updateUserInfo(state, userInfo){
      state.userInfo = userInfo
    },
    updateDingConfig(state, dingConfig){
      state.dingConfig = dingConfig
    },
    updateUnPermDingIds(state, unPermDingIds){
      state.unPermDingIds = unPermDingIds
    },
    updateLeaders(state, leaders){
      state.leaders = leaders
    },
    updateLastLoginTime(state, loginTime){
      state.lastLoginTime = loginTime
    },
  }
}
