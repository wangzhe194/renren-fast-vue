export default {
  namespaced: true,
  state: {
    voteId:1,
  },
  mutations: {
    updateVoteId(state, voteId){
      state.voteId = voteId
    }
  }
}
