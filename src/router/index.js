/**
 * 全站路由配置
 *
 * 建议:
 * 1. 代码中路由统一使用name属性跳转(不使用path属性)
 */
import Vue from 'vue'
import Router from 'vue-router'
import http from '@/utils/httpRequest'
import { isURL } from '@/utils/validate'
import { clearLoginInfo } from '@/utils'
import * as dd from 'dingtalk-jsapi'

Vue.use(Router)

// 开发环境不使用懒加载, 因为懒加载页面太多的话会造成webpack热更新太慢, 所以只有生产环境使用懒加载
const _import = require('./import-' + process.env.NODE_ENV)

// 全局路由(无需嵌套上左右整体布局)
const globalRoutes = [
  { path: '/404', component: _import('common/404'), name: '404', meta: { title: '404未找到' } },
  { path: '/login', component: _import('common/login'), name: 'login', meta: { title: '登录' } },
  { path: '/erp-thing', component: _import('modules/erp/erpthing'), name: 'erp-thing', meta: { title: 'ERP编码' } },
  { path: '/liv-user', component: _import('modules/sys/liv_user'), name: 'liv-user', meta: { title: '用户管理' } },
  { path: '/app-thing', component: _import('modules/generator/appthing'), name: 'app-thing', meta: { title: '物品编码' } },
  { path: '/app-wpb', component: _import('modules/wpb/app-wpb'), name: 'app-wpb', meta: { title: '工作进度面板' },
    children: [
      { path: '/wpb-project', component: _import('modules/wpb/wpb-project'), name: 'wpb-project', meta: { title: '项目' } },
      { path: '/wpb-board', component: _import('modules/wpb/wpb-board'), name: 'wpb-board', meta: { title: '看板' } },
      { path: '/wpb-dyn', component: _import('modules/wpb/wpb-dyn'), name: 'wpb-dyn', meta: { title: '动态' } },
      { path: '/wpb-log', component: _import('modules/wpb/wpb-log'), name: 'wpb-log', meta: { title: '日志' } }
    ]
  },
  { path: '/wpb-add', component: _import('modules/wpb/wpb-add'), name: 'wpb-add', meta: { title: '添加项目' } },
  { path: '/wpb-log-detail', component: _import('modules/wpb/wpb-log-detail'), name: 'wpb-log-detail', meta: { title: '项目日志详情' } },
  { path: '/log-mark', component: _import('modules/wpb/log-mark'), name: 'log-mark', meta: { title: '日志详情' } },

  { path: '/vote-base', component: _import('modules/vote/vote-base'), name: 'vote-base', meta: { title: '力沃投票管理' },
    children: [
      { path: '/vote-history', component: _import('modules/vote/vote-history'), name: 'vote-history', meta: { title: '操作历史' } },
      { path: '/vote-setting', component: _import('modules/vote/vote-setting'), name: 'vote-setting', meta: { title: '投票设置' } },
      { path: '/vote-static', component: _import('modules/vote/vote-static'), name: 'vote-static', meta: { title: '统计查看' } }
    ]
  },
  { path: '/vote-app', component: _import('modules/vote/vote-app'), name: 'vote-app', meta: { title: '力沃投票' } },
  { path: '/app-reminder', component: _import('modules/app/reminder'), name: 'app-reminder', meta: { title: '力沃提醒' } },
  { path: '/task-list', component: _import('modules/tasklist/tasks'), name: 'task-list', meta: { title: '任务清单' }
  },
  { path: '/mbbs', component: _import('modules/bbs-mobile/index'), name: 'mbbs', meta: { title: '力沃社区' },
    children: [
      { path: '/mbbs-home', component: _import('modules/bbs-mobile/home'), name: 'mbbs-home', meta: { title: '首页', keepAlive: true } },
      { path: '/mbbs-announcement', component: _import('modules/bbs-mobile/announcement'), name: 'mbbs-announcement', meta: { title: '公告', keepAlive: true } },
      { path: '/mbbs-library', component: _import('modules/bbs-mobile/library'), name: 'mbbs-library', meta: { title: '文库', keepAlive: true } },
      { path: '/mbbs-discussion', component: _import('modules/bbs-mobile/discussion'), name: 'mbbs-discussion', meta: { title: '讨论', keepAlive: true } },
      { path: '/mbbs-unnormal', component: _import('modules/bbs-mobile/unnormal'), name: 'mbbs-unnormal', meta: { title: '异常', keepAlive: true } },
      { path: '/mbbs-article', component: _import('modules/bbs-mobile/article'), name: 'mbbs-article', meta: { title: '文章', keepAlive: false } },
      { path: '/mbbs-personal', component: _import('modules/bbs-mobile/personal'), name: 'mbbs-personal', meta: { title: '我的', keepAlive: true } },
      { path: '/mbbs-editor', component: _import('modules/bbs-mobile/editor'), name: 'mbbs-editor', meta: { title: '编辑', keepAlive: false } },
      { path: '/mbbs-module', component: _import('modules/bbs-mobile/module'), name: 'mbbs-module', meta: { title: '栏目管理', keepAlive: false } },
    ]
  },
  { path: '/nmpb', component: _import('modules/mpb/main'), name: 'nmpb', meta: { title: '销售看板' },
    children: [
      { path: '/nmpb-flsc', component: _import('modules/mpb/flsc'), name: 'nmpb-flsc', meta: { title: '分类市场', isTab: true } },
      { path: '/nmpb-ptyy', component: _import('modules/mpb/ptyy'), name: 'nmpb-ptyy', meta: { title: '平台运营', isTab: true } },
      { path: '/nmpb-pptg', component: _import('modules/mpb/pptg'), name: 'nmpb-pptg', meta: { title: '品牌推广', isTab: true } },
      { path: '/nmpb-tdcg', component: _import('modules/mpb/tdcg'), name: 'nmpb-tdcg', meta: { title: '团队成果', isTab: true } },
      { path: '/nmpb-ast', component: _import('modules/mpb/ast'), name: 'nmpb-ast', meta: { title: 'AST客户', isTab: true } },
      { path: '/nmpb-yjhz', component: _import('modules/mpb/yjhz'), name: 'nmpb-yjhz', meta: { title: '业绩汇总', isTab: true } },
      { path: '/nmpb-b2blr', component: _import('modules/mpb/b2blr'), name: 'nmpb-b2blr', meta: { title: 'B2B', isTab: true } },
      { path: '/nmpb-b2clr', component: _import('modules/mpb/b2clr'), name: 'nmpb-b2clr', meta: { title: 'B2C', isTab: true } },
      { path: '/nmpb-nryylr', component: _import('modules/mpb/nryylr'), name: 'nmpb-nryylr', meta: { title: '内容运营', isTab: true } },
      { path: '/nmpb-ggtglr', component: _import('modules/mpb/ggtglr'), name: 'nmpb-ggtglr', meta: { title: '广告推广', isTab: true } },
      { path: '/nmpb-jytglr', component: _import('modules/mpb/jytglr'), name: 'nmpb-jytglr', meta: { title: '交易推广', isTab: true } },
      { path: '/nmpb-xspt', component: _import('modules/mpb/xspt'), name: 'nmpb-xspt', meta: { title: '销售平台', isTab: true } },
      { path: '/nmpb-tgfl', component: _import('modules/mpb/tgfl'), name: 'nmpb-tgfl', meta: { title: '推广分类', isTab: true } },
      { path: '/nmpb-khfj', component: _import('modules/mpb/khfj'), name: 'nmpb-khfj', meta: { title: '客户分级', isTab: true } },
      { path: '/nmpb-scfj', component: _import('modules/mpb/scfj'), name: 'nmpb-scfj', meta: { title: '市场分级', isTab: true } },
      { path: '/nmpb-b2b', component: _import('modules/mpb/b2b'), name: 'nmpb-b2b', meta: { title: 'B2B团队', isTab: true } },
      { path: '/nmpb-hlsz', component: _import('modules/mpb/hlsz'), name: 'nmpb-hlsz', meta: { title: '汇率设置', isTab: true } },
      { path: '/nmpb-yhjs', component: _import('modules/mpb/yhjs'), name: 'nmpb-yhjs', meta: { title: '用户角色', isTab: true } },
    ]
  },
  { path: '/mmpb', component: _import('modules/mpb-mobile/index'), name: 'mmpb', meta: { title: '销售看板' },
    children: [
      { path: '/mmpb-yjhz', component: _import('modules/mpb-mobile/common'), name: 'mmpb-yjhz', meta: { title: '业绩汇总', type: 'yjhz'} },
      { path: '/mmpb-flsc', component: _import('modules/mpb-mobile/common'), name: 'mmpb-flsc', meta: { title: '分类市场', type: 'flsc' } },
      { path: '/mmpb-ptyy', component: _import('modules/mpb-mobile/common'), name: 'mmpb-ptyy', meta: { title: '平台运营', type: 'ptyy' } },
      { path: '/mmpb-tdcg', component: _import('modules/mpb-mobile/common'), name: 'mmpb-tdcg', meta: { title: '团队成果', type: 'tdcg' } },
      { path: '/mmpb-detail', component: _import('modules/mpb-mobile/common'), name: 'mmpb-detail'},
      { path: '/mmpb-deepDetail', component: _import('modules/mpb-mobile/common'), name: 'mmpb-deepDetail'}
    ]
  },
  { path: '/livolobim', component: _import('modules/livolobim/index'), name: 'livolobim', meta: { title: '力沃分工' },
    children: [
      { path: '/livolobim-task', component: _import('modules/livolobim/task'), name: 'livolobim-task', meta: { title: '任务' } },
      { path: '/livolobim-module', component: _import('modules/livolobim/module'), name: 'livolobim-module', meta: { title: '版块' } },
      { path: '/livolobim-project', component: _import('modules/livolobim/project'), name: 'livolobim-project', meta: { title: '项目' } },
      { path: '/livolobim-statistics', component: _import('modules/livolobim/statistics'), name: 'livolobim-statistics', meta: { title: '统计' } },
      { path: '/livolobim-trends', component: _import('modules/livolobim/trends'), name: 'livolobim-trends', meta: { title: '动态' } }
    ]
  },
  { path: '/mlivolobim', component: _import('modules/livolobim-mobile/index'), name: 'mlivolobim', meta: { title: '力沃分工' },
    children: [
      { path: '/mlivolobim-task', component: _import('modules/livolobim-mobile/task'), name: 'mlivolobim-task', meta: { title: '任务', keepAlive: true } },
      { path: '/mlivolobim-module', component: _import('modules/livolobim-mobile/module'), name: 'mlivolobim-module', meta: { title: '版块', keepAlive: true } },
      { path: '/mlivolobim-project', component: _import('modules/livolobim-mobile/project'), name: 'mlivolobim-project', meta: { title: '项目', keepAlive: true } },
      { path: '/mlivolobim-statistics', component: _import('modules/livolobim-mobile/statistics'), name: 'mlivolobim-statistics', meta: { title: '统计' } },
      { path: '/mlivolobim-trends', component: _import('modules/livolobim-mobile/trends'), name: 'mlivolobim-trends', meta: { title: '动态', keepAlive: true} },
      { path: '/mlivolobim-addTask', component: _import('modules/livolobim-mobile/add-task-page'), name: 'mlivolobim-addTask', meta: { title: '新增任务' } },
      { path: '/mlivolobim-taskDetail', component: _import('modules/livolobim-mobile/task-detail'), name: 'mlivolobim-taskDetail', meta: { title: '任务详情' } },
      { path: '/mlivolobim-projectDetail', component: _import('modules/livolobim-mobile/project-detail'), name: 'mlivolobim-projectDetail', meta: { title: '项目详情' } },
      { path: '/mlivolobim-addOrUpdateProject', component: _import('modules/livolobim-mobile/add-or-update-project'), name: 'mlivolobim-addOrUpdateProject', meta: { title: '新增编辑项目' } },
      { path: '/mlivolobim-moduleDetail', component: _import('modules/livolobim-mobile/module-detail'), name: 'mlivolobim-moduleDetail', meta: { title: '版块详情' } },
      { path: '/mlivolobim-addOrUpdateModule', component: _import('modules/livolobim-mobile/add-or-update-module'), name: 'mlivolobim-addOrUpdateModule', meta: { title: '新增编辑版块' } },
      { path: '/mlivolobim-relationGraph', component: _import('modules/livolobim-mobile/relation-graph'), name: 'mlivolobim-relationGraph', meta: { title: '项目流程图' } },
    ]
  },
  { path: '/mlkr', component: _import('modules/lkr-mobile/index'), name: 'mlkr', meta: { title: 'mlkr' },
    children: [
      { path: '/mlkr-home', component: _import('modules/lkr-mobile/home'), name: 'mlkr-home', meta: { title: 'LKR首页', keepAlive: true} },
      { path: '/mlkr-detail', component: _import('modules/lkr-mobile/detail'), name: 'mlkr-detail', meta: { title: '详情'} },
      { path: '/mlkr-statistics', component: _import('modules/lkr-mobile/statistics'), name: 'mlkr-statistics', meta: { title: '统计'} }
    ]
  },
  { path: '/lkr', component: _import('modules/lkr/index'), name: 'lkr', meta: { title: '力沃LKR' },
    children: [
      { path: '/lkr-okrs', component: _import('modules/lkr/okrs'), name: 'lkr-okrs', meta: { title: 'OKRs' } },
      { path: '/lkr-task', component: _import('modules/lkr/task'), name: 'lkr-task', meta: { title: '我的任务' } },
      { path: '/lkr-dataPanel', component: _import('modules/lkr/data-panel'), name: 'lkr-data-panel', meta: { title: '数据看板' } },
    ]
  },
  { path: '/lcrm', component: _import('modules/lcrm/index'), name: 'lcrm', meta: { title: '力沃LKR' },
    children: [
      { path: '/lcrm-dbsx', component: _import('modules/lcrm/crm/dbsx'), name: 'lcrm-dbsx', meta: { title: 'OKRs' } },
      { path: '/lcrm-lxgj', component: _import('modules/lcrm/crm/lxgj'), name: 'lcrm-lxgj', meta: { title: 'OKRs' } },
      { path: '/lcrm-qbkh', component: _import('modules/lcrm/crm/qbkh'), name: 'lcrm-qbkh', meta: { title: 'OKRs' } },
      { path: '/lcrm-wdkh', component: _import('modules/lcrm/crm/wdkh'), name: 'lcrm-wdkh', meta: { title: 'OKRs' } },
      { path: '/lcrm-gxkh', component: _import('modules/lcrm/crm/gxkh'), name: 'lcrm-gxkh', meta: { title: 'OKRs' } },
      { path: '/lcrm-ghkh', component: _import('modules/lcrm/crm/ghkh'), name: 'lcrm-ghkh', meta: { title: 'OKRs' } },
      { path: '/lcrm-ddgl', component: _import('modules/lcrm/crm/ddgl'), name: 'lcrm-ddgl', meta: { title: 'OKRs' } },
      { path: '/lcrm-khts', component: _import('modules/lcrm/crm/khts'), name: 'lcrm-khts', meta: { title: 'OKRs' } },
      { path: '/lcrm-zy', component: _import('modules/lcrm/crm/zy'), name: 'lcrm-zy', meta: { title: 'OKRs' } },
      { path: '/lcrm-cpgl', component: _import('modules/lcrm/cp/cpgl'), name: 'lcrm-cpgl', meta: { title: 'OKRs' } },
      { path: '/lcrm-xtsz', component: _import('modules/lcrm/sz/xtsz'), name: 'lcrm-xtsz', meta: { title: 'OKRs' } },
      { path: '/lcrm-qxsz', component: _import('modules/lcrm/sz/qxsz'), name: 'lcrm-qxsz', meta: { title: 'OKRs' } },
      { path: '/lcrm-email', component: _import('modules/lcrm/email/list'), name: 'lcrm-list', meta: { title: 'OKRs' } },
      { path: '/lcrm-lxr', component: _import('modules/lcrm/email/lxr'), name: 'lcrm-lxr', meta: { title: 'OKRs' } },
      { path: '/lcrm-zh', component: _import('modules/lcrm/email/zh'), name: 'lcrm-zh', meta: { title: 'OKRs' } },
      { path: '/lcrm-khfj', component: _import('modules/lcrm/tb/khfj'), name: 'lcrm-khfj', meta: { title: 'OKRs' } },
      { path: '/lcrm-tdtj', component: _import('modules/lcrm/tb/tdtj'), name: 'lcrm-tdtj', meta: { title: 'OKRs' } },
      { path: '/lcrm-cpxs', component: _import('modules/lcrm/tb/cpxs'), name: 'lcrm-cpxs', meta: { title: 'OKRs' } },
      { path: '/lcrm-gslb', component: _import('modules/lcrm/crm/gslb'), name: 'lcrm-gslb', meta: { title: 'OKRs' } },
    ]
  },
  { path: '/piecework', component: _import('modules/piecework/index'), name: 'piecework', meta: { title: '计件' },
    children: [
      { path: '/piecework-uncompleteList', component: _import('modules/piecework/handle-list/uncompleteList'), name: 'piecework-uncompleteList', meta: { title: '待处理' } },
      { path: '/piecework-myApplicationList', component: _import('modules/piecework/handle-list/myApplicationList'), name: 'piecework-myApplicationList', meta: { title: '我发起' } },
      { path: '/piecework-storageList', component: _import('modules/piecework/storage/list'), name: 'piecework-storageList', meta: { title: '计件入库' } },
      { path: '/piecework-fixedAssetslist', component: _import('modules/piecework/fixed-assets/list'), name: 'piecework-fixedAssetslist', meta: { title: '固定资产' } },
      { path: '/piecework-productList', component: _import('modules/piecework/product/list'), name: 'piecework-productList', meta: { title: '产品管理' } },
      { path: '/piecework-productionPlanningManagementList', component: _import('modules/piecework/production-planning-management/list'), name: 'piecework-productionPlanningManagementList', meta: { title: '生产计划管理' } },
      { path: '/piecework-settingLimits', component: _import('modules/piecework/setting/limits'), name: 'piecework-settingLimits', meta: { title: '设置' } },
      { path: '/piecework-settingArugment', component: _import('modules/piecework/setting/arugment'), name: 'piecework-settingArugment', meta: { title: '设置' } },
      { path: '/piecework-settingCraft', component: _import('modules/piecework/setting/craft'), name: 'piecework-settingCraft', meta: { title: '设置' } },
      { path: '/piecework-trends', component: _import('modules/piecework/trends'), name: 'piecework-trends', meta: { title: '动态' } },
    ]
  },
  { path: '/mpiecework', component: _import('modules/piecework-mobile/index'), name: 'mpiecework', meta: { title: '计件' },
    children: [
      { path: '/mpiecework-uncompleteList', component: _import('modules/piecework-mobile/uncompleteList'), name: 'mpiecework-uncompleteList', meta: { title: '待处理', keepAlive: true } },
      { path: '/mpiecework-storageList', component: _import('modules/piecework-mobile/storage/list'), name: 'mpiecework-storageList', meta: { title: '计件入库功能模块', keepAlive: true } },
      { path: '/mpiecework-storageAddOrEdit', component: _import('modules/piecework-mobile/storage/add-or-edit'), name: 'mpiecework-storageAddOrEdit', meta: { title: '计件入库' } },
      { path: '/mpiecework-productionPlanningManagementList', component: _import('modules/piecework-mobile/production-planning-management/list'), name: 'mpiecework-productionPlanningManagementList', meta: { title: '生产计划管理', keepAlive: true } },
      { path: '/mpiecework-piece', component: _import('modules/piecework-mobile/production-planning-management/Piece'), name: 'mpiecework-piece', meta: { title: '计件', keepAlive: true } },
    ]
  },
  { path: '/letter', component: _import('modules/letter/index'), name: 'letter', meta: { title: '信访APP' },
    children: [
      { path: '/letter-rules', component: _import('modules/letter/rules'), name: 'letter-rules', meta: { title: '信访APP' } },
      { path: '/letter-add', component: _import('modules/letter/add'), name: 'letter-add', meta: { title: '信访APP' } },
      { path: '/letter-list', component: _import('modules/letter/list'), name: 'letter-list', meta: { title: '信访APP', keepAlive: true} },
      { path: '/letter-handleList', component: _import('modules/letter/handle-list'), name: 'letter-handleList', meta: { title: '信访APP', keepAlive: true } },
      { path: '/letter-mobileList', component: _import('modules/letter/mobile-list'), name: 'letter-mobileList', meta: { title: '信访APP', keepAlive: true } },
      { path: '/letter-mobileHandleList', component: _import('modules/letter/mobile-handle-list'), name: 'letter-mobileHandleList', meta: { title: '信访APP', keepAlive: true } },
      { path: '/letter-setting', component: _import('modules/letter/setting'), name: 'letter-setting', meta: { title: '信访APP' } },
    ]
  },
  { path: '/help', component: _import('modules/help/help'), name: 'help', meta: { title: '帮助手册' }},
  { path: '/sign', component: _import('modules/sign/index'), name: 'sign', meta: { title: '技术文件签名' },
    children: [
      { path: '/sign-list', component: _import('modules/sign/list'), name: 'sign-list', meta: { title: '', keepAlive: true } },
      { path: '/sign-create', component: _import('modules/sign/create'), name: 'sign-create', meta: { title: '' } },
      { path: '/sign-history', component: _import('modules/sign/history'), name: 'sign-history', meta: { title: '' } },
      { path: '/sign-setting', component: _import('modules/sign/setting'), name: 'sign-setting', meta: { title: '' } },
      { path: '/sign-detail', component: _import('modules/sign/detail'), name: 'sign-detail', meta: { title: '' } },
    ]
  },
  { path: '/administration', component: _import('modules/administration/index'), name: 'administration', meta: { title: '人事管理系统' },
    children: [
      { path: '/administration-recruitList', component: _import('modules/administration/recruit/list'), name: 'administration-recruitList', meta: { title: '招聘列表' } },
      { path: '/administration-recruitSetting', component: _import('modules/administration/recruit/setting'), name: 'administration-recruitSetting', meta: { title: '招聘设置' } },
      { path: '/administration-recruitModel', component: _import('modules/administration/recruit/model'), name: 'administration-recruitModel', meta: { title: '面试流程步骤' } },
      { path: '/administration-employeeList', component: _import('modules/administration/personel-document/employee-list'), name: 'administration-employeeList', meta: { title: '花名册' } },
      { path: '/administration-formerEmployee', component: _import('modules/administration/personel-document/former-employee'), name: 'administration-formerEmployee', meta: { title: '离职员工' } },
      { path: '/administration-personalInformation', component: _import('modules/administration/personel-document/personal-information'), name: 'administration-personalInformation', meta: { title: '个人信息' } },
      { path: '/administration-attendanceList', component: _import('modules/administration/personel-document/attendance-list'), name: 'administration-attendanceList', meta: { title: '考勤列表' } },
      { path: '/administration-integralDividend', component: _import('modules/administration/rewards-punishments/integral-dividend'), name: 'administration-integralDividend', meta: { title: '积分红利' } },
      { path: '/administration-rewardsList', component: _import('modules/administration/rewards-punishments/rewards-list'), name: 'administration-rewardsList', meta: { title: '力沃奖惩' } },
      { path: '/administration-performanceList', component: _import('modules/administration/rewards-punishments/performance-list'), name: 'administration-performanceList', meta: { title: '绩效列表' } },
      { path: '/administration-assetsList', component: _import('modules/administration/fixed-assets/assets-list'), name: 'administration-assetsList', meta: { title: '资产列表' } },
      { path: '/administration-assetsSetting', component: _import('modules/administration/fixed-assets/assets-setting'), name: 'administration-assetsSetting', meta: { title: '资产设定' } },
      { path: '/administration-rightsSetting', component: _import('modules/administration/setting/rights-setting'), name: 'administration-rightsSetting', meta: { title: '权限设置' } },
      { path: '/administration-log', component: _import('modules/administration/setting/log'), name: 'administration-log', meta: { title: '操作日志' } },
      { path: '/administration-argumentSetting', component: _import('modules/administration/setting/argument-setting'), name: 'administration-argumentSetting', meta: { title: '参数设置' } },
    ]
  },
  { path: '/bms', component: _import('modules/bms/index'), name: 'bms', meta: { title: '智能产品后台运营中心' },
    children: [
      { path: '/bms-statistics', component: _import('modules/bms/statistics'), name: 'bms-statistics', meta: { title: '概览', keepAlive: true } },
      { path: '/bms-switchList', component: _import('modules/bms/switch/list'), name: 'bms-switchList', meta: { title: '设备列表', keepAlive: true } },
      { path: '/bms-switchActive', component: _import('modules/bms/switch/active'), name: 'bms-switchActive', meta: { title: '活跃设备', keepAlive: true } },
      { path: '/bms-switchDetail', component: _import('modules/bms/switch/detail'), name: 'bms-switchDetail', meta: { title: '设备详情' } },
      { path: '/bms-newSwitch', component: _import('modules/bms/switch/new'), name: 'bms-newSwitch', meta: { title: '新增设备', keepAlive: true } },
      { path: '/bms-userList', component: _import('modules/bms/user/list'), name: 'bms-userList', meta: { title: '用户列表', keepAlive: true } },
      { path: '/bms-userActive', component: _import('modules/bms/user/active'), name: 'bms-userActive', meta: { title: '活跃用户', keepAlive: true } },
      { path: '/bms-userDetail', component: _import('modules/bms/user/detail'), name: 'bms-userDetail', meta: { title: '用户详情' } },
      { path: '/bms-userFeedback', component: _import('modules/bms/user/feedback'), name: 'bms-userFeedback', meta: { title: '用户反馈', keepAlive: true } },
      { path: '/bms-newUser', component: _import('modules/bms/user/new'), name: 'bms-newUser', meta: { title: '新增用户', keepAlive: true } },
      { path: '/bms-setting', component: _import('modules/bms/setting'), name: 'bms-setting', meta: { title: '权限设置' } },
      { path: '/bms-emailList', component: _import('modules/bms/email/list'), name: 'bms-emailList', meta: { title: '邮件列表' } },
      { path: '/bms-messageList', component: _import('modules/bms/email/message-list'), name: 'bms-messageList', meta: { title: '草稿列表' } },
      { path: '/bms-senderList', component: _import('modules/bms/email/sender-list'), name: 'bms-senderList', meta: { title: '发件人列表' } },
    ]
  },
  { path: '/mproject', component: _import('modules/project-mobile/index'), name: 'mproject', meta: { title: 'PDE项目考核' },
    children: [
      { path: '/mproject-list', component: _import('modules/project-mobile/list'), name: 'mproject-list', meta: { title: '项目列表', keepAlive: true } },
      { path: '/mproject-assess', component: _import('modules/project-mobile/assess'), name: 'mproject-assess', meta: { title: '评分列表'} },
      { path: '/mproject-check', component: _import('modules/project-mobile/check'), name: 'mproject-check', meta: { title: '审核列表' } },
      { path: '/mproject-score', component: _import('modules/project-mobile/score'), name: 'mproject-score', meta: { title: '项目评分' } },
      { path: '/mproject-project', component: _import('modules/project-mobile/project'), name: 'mproject-project', meta: { title: '项目详情' } },
    ]
  },
]

// 主入口路由(需嵌套上左右整体布局)
const mainRoutes = {
  path: '/',
  component: _import('main'),
  name: 'main',
  redirect: { name: 'home' },
  meta: { title: '主入口整体布局' },
  children: [
    // 通过meta对象设置路由展示方式
    // 1. isTab: 是否通过tab展示内容, true: 是, false: 否
    // 2. iframeUrl: 是否通过iframe嵌套展示内容, '以http[s]://开头': 是, '': 否
    // 提示: 如需要通过iframe嵌套展示内容, 但不通过tab打开, 请自行创建组件使用iframe处理!
    { path: '/home', component: _import('common/home'), name: 'home', meta: { title: '首页' } },
    { path: '/theme', component: _import('common/theme'), name: 'theme', meta: { title: '主题' } }
/*    { path: '/demo-echarts', component: _import('demo/echarts'), name: 'demo-echarts', meta: { title: 'demo-echarts', isTab: true } },
    { path: '/demo-ueditor', component: _import('demo/ueditor'), name: 'demo-ueditor', meta: { title: 'demo-ueditor', isTab: true } }*/
  ],
  beforeEnter (to, from, next) {
    let token = Vue.cookie.get('token')
    if (!token || !/\S/.test(token)) {
      clearLoginInfo()
      next({ name: 'login' })
    }
    next()
  }
}

const router = new Router({
  mode: 'hash',
  scrollBehavior: () => ({ y: 0 }),
  isAddDynamicMenuRoutes: false, // 是否已经添加动态(菜单)路由
  routes: globalRoutes.concat(mainRoutes)
})

router.beforeEach((to, from, next) => {
  // 添加动态(菜单)路由
  // 1. 已经添加 or 全局路由, 直接访问
  // 2. 获取菜单列表, 添加并保存本地存储
  if (router.options.isAddDynamicMenuRoutes || fnCurrentRouteType(to, globalRoutes) === 'global') {
    next()
  } else {
    http({
      url: http.adornUrl('/sys/menu/nav'),
      method: 'get',
      params: http.adornParams()
    }).then(({data}) => {
      if (data && data.code === 0) {
        fnAddDynamicMenuRoutes(data.menuList)
        router.options.isAddDynamicMenuRoutes = true
        sessionStorage.setItem('menuList', JSON.stringify(data.menuList || '[]'))
        sessionStorage.setItem('permissions', JSON.stringify(data.permissions || '[]'))
        next({ ...to, replace: true })
      } else {
        sessionStorage.setItem('menuList', '[]')
        sessionStorage.setItem('permissions', '[]')
        next()
      }
    }).catch((e) => {
      console.log(`%c${e} 请求菜单列表和权限失败，跳转至登录页！！`, 'color:blue')
      router.push({ name: 'login' })
    })
  }
})

/**
 * 判断当前路由类型, global: 全局路由, main: 主入口路由
 * @param {*} route 当前路由
 */
function fnCurrentRouteType (route, globalRoutes = []) {
  var temp = []
  for (var i = 0; i < globalRoutes.length; i++) {
    if (route.path === globalRoutes[i].path) {
      return 'global'
    } else if (globalRoutes[i].children && globalRoutes[i].children.length >= 1) {
      temp = temp.concat(globalRoutes[i].children)
    }
  }
  return temp.length >= 1 ? fnCurrentRouteType(route, temp) : 'main'
}

/**
 * 添加动态(菜单)路由
 * @param {*} menuList 菜单列表
 * @param {*} routes 递归创建的动态(菜单)路由
 */
function fnAddDynamicMenuRoutes (menuList = [], routes = []) {
  var temp = []

  for (var i = 0; i < menuList.length; i++) {
    if (menuList[i].list && menuList[i].list.length >= 1) {
      temp = temp.concat(menuList[i].list)
    } else if (menuList[i].url && /\S/.test(menuList[i].url)) {
      menuList[i].url = menuList[i].url.replace(/^\//, '')
      var route = {
        path: menuList[i].url.replace('/', '-'),
        component: null,
        name: menuList[i].url.replace('/', '-'),
        meta: {
          menuId: menuList[i].menuId,
          title: menuList[i].name,
          isDynamic: true,
          isTab: true,
          iframeUrl: ''
        }
      }
      // url以http[s]://开头, 通过iframe展示
      if (isURL(menuList[i].url)) {
        route['path'] = `i-${menuList[i].menuId}`
        route['name'] = `i-${menuList[i].menuId}`
        route['meta']['iframeUrl'] = menuList[i].url
      } else {
        try {
          route['component'] = _import(`modules/${menuList[i].url}`) || null
        } catch (e) {}
      }
      routes.push(route)
    }
  }

  if (temp.length >= 1) {
    fnAddDynamicMenuRoutes(temp, routes)
  } else {
    mainRoutes.name = 'main-dynamic'
    mainRoutes.children = routes
    router.addRoutes([
      mainRoutes,
      { path: '*', redirect: { name: '404' } }
    ])
    sessionStorage.setItem('dynamicMenuRoutes', JSON.stringify(mainRoutes.children || '[]'))
    console.log('\n')
    console.log('%c!<-------------------- 动态(菜单)路由 s -------------------->', 'color:blue')
    console.log(mainRoutes.children)
    console.log('%c!<-------------------- 动态(菜单)路由 e -------------------->', 'color:blue')
  }
}

export default router
