export default {
  data() {
    return {
      poPlats:[]
    };
  },
  created(){
    this.getPoPlats()
  },
  methods: {
    isPlatUser(platId, dingId){
      let us = this.poPlats.filter((item)=>{
        let res = item.id==platId && item.remark==dingId
        console.log(dingId + " isPlatUser " + item.id)
        return res 
      })
      if(us.length>0){
        return true
      }
      return false
    },
    getPoPlats(){
      this.$http({
        url: this.$http.adornUrl('/sale/listPoPlat'),
        method: 'get',
        params: this.$http.adornParams({})
      }).then(({data}) => {
        if (data && data.code === 0) {
          this.poPlats = data.data
        } else {
          this.poPlats = []
        }
      })
    },
  },
  computed:{

  },
}
