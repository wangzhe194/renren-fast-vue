import { getMyRolePerm} from '@/api/personal/personal.js'

export default {
  props: {
    permission:''
  },
  data() {
    return {
      display:false,
      perm:""
    };
  },
  methods: {
    validPermision(perms){
      let p = this.permission
      for(let i=0; i<perms.length; i++)
      {
        //console.log('permission:'+p)
        //console.log('role perm:'+ perms[i].perm);
        if(perms[i].perm == p){
          this.display = true
          return;
        }
      }
      this.display = false
    },
    isHasPermission(role) {
      if(!role){
        return true;
      }
      return this.perm.find(permission => {
        return permission.perm == role
      })
    }
  },

  created(){
    //先从缓存里取权限数据，如果没命中， 从接口里取数据
    this.perm=JSON.parse(localStorage.getItem('perm'))
    if(!this.perm){
      getMyRolePerm({}).then(res => {
        if(res.data.code == 200) {
          localStorage.setItem('perm',JSON.stringify(res.data.data))
          this.perm=res.data.data
          this.validPermision(res.data.data)
        }else{
          this.$message.error(res.data.message);
        }
      })
    }
    else
      this.validPermision(this.perm)
  }
}
