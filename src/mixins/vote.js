export default {
  data() {
    return {

    };
  },
  created(){

  },
  methods: {

  },
  computed:{
    voteId: {
      get () { return this.$store.state.vote.voteId },
      set (val) { this.$store.commit('vote/updateVoteId', val)  }
    },
  },
}
