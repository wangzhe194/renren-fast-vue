Date.prototype.Format = function (fmt) { //author: meizz
  var o = {
    "M+": this.getMonth() + 1, //月份
    "d+": this.getDate(), //日
    "h+": this.getHours(), //小时
    "m+": this.getMinutes(), //分
    "s+": this.getSeconds(), //秒
    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
    "S": this.getMilliseconds() //毫秒
  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

export const dateUtil = {

  parse(str){
    str = str.replace(/-/g, '/');
    let date = new Date(str);
    return date;
  },


  thisWeekBegin(now) {

    let prex = getLastMonth(now).last;
    //alert(now);
    let nowDate = new Date(Date.parse(prex));
    let nowTime = nowDate.getTime();
    let day = nowDate.getDay();
    let oneDayLong = 24 * 60 * 60 * 1000;
    let MondayTime = nowTime - (day - 1) * oneDayLong;
    let monday = new Date(MondayTime);
    return monday.Format("yyyy-MM-dd");
  },

  thisWeekEnd(now) {

    let nowTime = now.getTime();
    let day = now.getDay();
    let oneDayLong = 24 * 60 * 60 * 1000;
    let mondayTime = nowTime - (day - 1) * oneDayLong;
    let sunday = new Date(mondayTime + 6 * oneDayLong);
    return sunday.Format("yyyy-MM-dd");
  },

  getLastMonth(now) {

    let year = now.getFullYear();//getYear()+1900=getFullYear()
    let month = now.getMonth() + 1;//0-11表示1-12月
    let day = now.getDate();
    let dateObj = {};
    if (parseInt(month) < 10) {
      month = "0" + month;
    }
    if (parseInt(day) < 10) {
      day = "0" + day;
    }

    dateObj.now = year + '-' + month + '-' + day;

    if (parseInt(month) == 1) {//如果是1月份，则取上一年的12月份
      dateObj.last = (parseInt(year) - 1) + '-12-' + day;
      return dateObj;
    }

    let preSize = new Date(year, parseInt(month) - 1, 0).getDate();//上月总天数
    if (preSize < parseInt(day)) {//上月总天数<本月日期，比如3月的30日，在2月中没有30
      dateObj.last = year + '-' + month + '-01';
      return dateObj;
    }

    if (parseInt(month) <= 10) {
      dateObj.last = year + '-0' + (parseInt(month) - 1) + '-' + day;
      return dateObj;
    } else {
      dateObj.last = year + '-' + (parseInt(month) - 1) + '-' + day;
      return dateObj;
    }
  },

  thisYearBegin() {
    let now = new Date();
    let year = now.getFullYear();
    return year + '-01-01';
  },

  thisYearEnd() {
    let now = new Date();
    let year = now.getFullYear();
    return year + '-12-31';
  },

  thisDay() {
    return (new Date()).Format("yyyy-MM-dd");
  },

  thisMonth() {
    return (new Date()).Format("yyyy-MM");
  },

  halfYearThisDay() {
    return new Date((new Date()).getTime() - 1000 * 3600 * 24 * 180).Format("yyyy-MM-dd");
  },

  nDaysBefore(n){
    return new Date((new Date()).getTime() - 1000 * 3600 * 24 * n).Format("yyyy-MM-dd");
  },

  nDayExpire(n, d){
    if(d == null || d=='')
      return false
    return d < this.nDaysBefore(n)
  },

  getWeek(date) {
    let _date = new Date(date);
    let week = 0;
    // getDay() 返回表示星期的某一天
    let num = _date.getDay();
    switch (num) {
      case 0:
        week = "周日";
        break;
      case 1:
        week = "周一";
        break;
      case 2:
        week = "周二"
        break;
      case 3:
        week = "周三"
        break;
      case 4:
        week = "周四"
        break;
      case 5:
        week = "周五"
        break;
      case 6:
        week = "周六"
        break;
      default:
        break;
    };
    return week;
  },

  getDefaultDate(type,offset){
    if(type==1){
      return this.nDaysBefore(0);
    }
    else if(type==2){
      return this.nDaysBefore(0);
    }
    else if(type==3){
      return this.getLastMonth(new Date()).last.substr(0,7);
    }
    return ''
  }
  
}
