import {Notification, Message, MessageBox} from 'element-ui';
import cloneDeep from 'lodash/cloneDeep'
import * as dd from 'dingtalk-jsapi'

let isPc = () => {
  let userAgentInfo = navigator.userAgent;
  let Agents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];
  let flag = true;
  for (let v = 0; v < Agents.length; v++) {
    if (userAgentInfo.indexOf(Agents[v]) > 0) {
      flag = false;
      break;
    }
  }
  return flag;
}


export default {
  /**
   * 表单提交后显示成功提示
   * @param {string} msg 
   * @param {string} title 
   */
  submitSuccess(msg, title = '成功') {
    if(isPc()) {
      Notification({
        title: title,
        message: msg,
        type: 'success'
      })
    }else {
      dd.device.notification.toast({
        icon: 'success', //icon样式，不同客户端参数不同，请参考参数说明
        text: msg, //提示信息
        duration: 1, //显示持续时间，单位秒，默认按系统规范[android只有两种(<=2s >2s)]
      })
    }
  },

  /**
   * 表单操作确认提示
   * @param {string} content 
   * @param {Function} callback 
   * @param {string} title 
   */
  submitConfirm(content, callback, title = '提示', closeFun) {
    if(isPc()) {
      MessageBox.confirm(content, title, {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning',
        center: true
      }).then(() => {
         callback()
      }).catch(() => {
        closeFun && closeFun()       
      });
    }else {
      dd.device.notification.confirm({
        message: content,
        title: title,
        buttonLabels: ['确定', '取消'],
        onSuccess : function(result) {
          if(result.buttonIndex === 0) {
            callback && callback()
          }
          if(result.buttonIndex === 1) {
            closeFun && closeFun()
          }
        },
      });
    }
  },

  submitError(msg, callback) {
    if(isPc()) {
      Message.error(msg)
    }else {
      dd.device.notification.alert({
        message: msg,
        title: "提示",//可传空
        buttonName: "确定",
        onSuccess : function() {
          callback && callback()
        }
      });
    }
  },

  /**
   * 重置所有表单属性,设置为空值
   * @param {Object} target 
   */
  resetData(target) {
    let toString = Object.prototype.toString;
    Object.keys(target).forEach((key) => {
      let value = target[key];
      if(toString.call(value) === '[object Object]') return this.resetData(value)
      target[key] = toString.call(value) === '[object String]' ? '' : 
                    toString.call(value) === '[object Number]' ? ''  :
                    toString.call(value) === '[object Array]'  ? [] : 
                    toString.call(value) === '[object Boolean]'? false : value
    })
  },

  /**
   * 表单验证并自动提示,返回true则为通过所有校验
   * target {id: 1, name: ''}
   * notice {id: '请输入id', name: '请输入姓名'}
   * @param {Object} target 
   * @param {Object} notice 
   */
  verifyData(target, notice) {
    let pass = true;
    Object.keys(notice).every((key) => {
      if(target[key] === '') {
        pass = false;
        Message.error(notice[key])
        return false
      }else {
        return true
      }
    })
    return pass
  },

  /**
   * 深拷贝
   * @param {Object} obj 
   */
  deepCopy(obj) {
    return cloneDeep(obj)
  }
}


