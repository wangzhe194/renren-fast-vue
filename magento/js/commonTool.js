(function(exprots){
    
    /**
     * 绑定监听事件 暂时先用click
     * @param {String} dom 单个dom,或者selector
     * @param {Function} callback 回调函数
     * @param {String} eventName 事件名
     */
    exprots.bindEvent = function(dom, callback, eventName) {
        eventName = eventName || 'click';
        if (typeof dom === 'string') {
            // 选择
            dom = document.querySelectorAll(dom);
        }
        if (!dom) {
            return;
        }
        if (dom.length > 0) {
            for (var i = 0, len = dom.length; i < len; i++) {
                dom[i].addEventListener(eventName, callback);
            }
        } else {
            dom.addEventListener(eventName, callback);
        }
    };
    
    
    //状态码转中文
    exprots.tarStatus=function(statId){
              if(statId==0){
                return '等待审批';
              }else if(statId==-1){
                  return '退回';
              }else if(statId==1){
                  return '进行中';
              }else{
                  return '完成';
              }  
            };
    
    exprots.isIntNumber=function(str){
        var re = /^[1-9]+[0-9]*]*$/;
　　     if (!re.test(str)) {
　　　　    return false;
　　          }
           return true;   
        }
    
    exprots.addTokenToParams=function(params){
        var token= $.cookie("tokenId");
        params.tokenId=token;
        return params;
    }
    

    
})(window.Tool={});