$(function(){
    //默认不显示看板内容
    $("#content2").hide();
    $(".setPer").hide();
    showLoading();
});
$.ajax({
    type: 'GET',
    url: baseUrl+"/interface/getConfig",
   xhrFields: {
   withCredentials: true
   },
   crossDomain: true,
    success: function(data){
       var _config=data;
       DingTalkPC.config({
    agentId : _config.agentid,
    corpId : _config.corpId,
    timeStamp : _config.timeStamp,
    nonceStr : _config.nonceStr,
    signature : _config.signature,
    jsApiList : ['biz.contact.choose',
        'device.notification.confirm', 'device.notification.alert',
        'device.notification.prompt', 'biz.ding.post',
        'biz.util.openLink' ]
});
       DingTalkPC.ready(function() {
           
//    DingTalkPC.biz.navigation.setTitle({
//        title: 'loading',
//        onSuccess: function(data) {
//        },
//        onFail: function(err) {
//            log.e(JSON.stringify(err));
//        }
//    });

   
 DingTalkPC.runtime.permission.requestAuthCode({
        corpId : _config.corpId,
        onSuccess : function(info) {
			alert('authcode: ' + info.code);
             $.ajax({
    		            type: 'POST',
    		            url: baseUrl+"/interface/index",
    		           contentType:"application/json;charset=UTF-8",
    		           data:JSON.stringify({'code':info.code,'corpid':_config.corpId}),
    		           xhrFields: {
    		           withCredentials: true
    		           },
    		           crossDomain: true,
    		            success: function(data){
    		            	//免登成功 此处进行页面跳转
                            var visitPer=false;
                            var adminStr='0';
                             if(typeof(data)=='string'){
                                var obj=JSON.parse(data);
                                $.cookie("tokenId",obj.data.tokenId);
                                var perStr=obj.data.visitPer;
                                adminStr=obj.data.isAdmin;
                                //$.cookie("isAdmin",adminStr);
                                if(perStr==1)
                                    visitPer=true;
                                else if(adminStr==1)
                                    visitPer=true;
                            }
                            else{
                                $.cookie("tokenId",data.data.tokenId);
                                var perStr=data.data.visitPer;
                                adminStr=data.data.isAdmin;
                                //$.cookie("isAdmin",adminStr);
                                if(perStr==1)
                                    visitPer=true;
                                else if(adminStr==1)
                                    visitPer=true;
                            } 
                                                   
                            if(visitPer&&adminStr==1){
                                $("#content2").show();
                                $(".setPer").show();
                            }else if(!visitPer&&adminStr==1){
                                $("#content2").show();
                                $(".setPer").show();
                            }else if(visitPer&&adminStr==0){
                                $("#content2").show();
                            }else{
                                $("body").append("<div id='ps' style='position:absolute;top:50%;width:100%;text-align:center'>您没有权限访问，请联系管理员</div>");
                            }    
    		            },error : function(xhr, errorType, error) {
    					    logger.e("yinyien:" + _config.corpId);
    					    alert(errorType + ', ' + error);
    				    }
    		    
    		       });

        },
        onFail : function(err) {
            alert('fail: ' + JSON.stringify(err));
        }
    });
});
           
    DingTalkPC.error(function(err) {
    alert('dd error: ' + JSON.stringify(err));
});

    },error : function(xhr, errorType, error) {
	    logger.e("yinyien:" + _config.corpId);
	    alert(errorType + ', ' + error);
    }
});



   
