$(function(){
    //默认不显示看板内容
    $("#content2").hide();
    $(".setPer").hide();
    showLoading();
});
$.ajax({
    type: 'GET',
    url: baseUrl+"/interface/getConfig",
    contentType:"application/json;charset=UTF-8",
   xhrFields: {
   withCredentials: true
   },
   crossDomain: true,
    success: function(data){
       var _config=data;
       dd.config({
			agentId : _config.agentid,
			corpId : _config.corpId,
			timeStamp : _config.timeStamp,
			nonceStr : _config.nonceStr,
			signature : _config.signature,
			jsApiList : [ 'runtime.info', 'biz.contact.choose',
					'device.notification.confirm']
		});
       dd.ready(function() {
    	    dd.biz.navigation.setTitle({
    	        title: '自建站统计看板',
    	        onSuccess: function(data) {
    	        },
    	        onFail: function(err) {
    	            log.e(JSON.stringify(err));
    	        }
    	    });
    		// alert('dd.ready rocks!');

    		dd.runtime.info({
    			onSuccess : function(info) {
                    console.log('runtime info: ' + JSON.stringify(info));
    				//logger.e('runtime info: ' + JSON.stringify(info));
    			},
    			onFail : function(err) {
    				logger.e('fail: ' + JSON.stringify(err));
    			}
    		});

    		dd.runtime.permission.requestAuthCode({
    			corpId : _config.corpId,
    			onSuccess : function(info) {
    		        $.ajax({
    		            type: 'POST',
    		            url: baseUrl+"/interface/index",
    		           contentType:"application/json;charset=UTF-8",
    		           data:JSON.stringify({'code':info.code,'corpid':_config.corpId}),
    		           xhrFields: {
    		           withCredentials: true
    		           },
    		           crossDomain: true,
    		            success: function(data){
    		            	//免登成功 此处进行页面跳转
                            var visitPer=false;
                            var adminStr='0';
                             if(typeof(data)=='string'){
                                var obj=JSON.parse(data);
                                $.cookie("tokenId",obj.data.tokenId);
                                var perStr=obj.data.visitPer;
                                adminStr=obj.data.isAdmin;
                                //$.cookie("isAdmin",adminStr);
                                if(perStr==1)
                                    visitPer=true;
                                else if(adminStr==1)
                                    visitPer=true;
                            }
                            else{
                                $.cookie("tokenId",data.data.tokenId);
                                var perStr=data.data.visitPer;
                                adminStr=data.data.isAdmin;
                                //$.cookie("isAdmin",adminStr);
                                if(perStr==1)
                                    visitPer=true;
                                else if(adminStr==1)
                                    visitPer=true;
                            } 
                                                   
                            if(visitPer&&adminStr==1){
                                $("#content2").show();
                                $(".setPer").show();
                            }else if(!visitPer&&adminStr==1){
                                $("#content2").show();
                                $(".setPer").show();
                            }else if(visitPer&&adminStr==0){
                                $("#content2").show();
                            }else{
                                $("body").append("<div id='ps' style='position:absolute;top:50%;width:100%;text-align:center'>您没有权限访问，请联系管理员</div>");
                            }        

    		               //window.location.href=visitPer?'chart.html':'index.html';
    		            },error : function(xhr, errorType, error) {
    					    logger.e("yinyien:" + _config.corpId);
    					    alert(errorType + ', ' + error);
    				    }
    		    
    		       });
    				
    			},
    			onFail : function(err) {
    				alert('fail: ' + JSON.stringify(err));
    			}
    		});
    		
    	});

    	dd.error(function(err) {
    		alert('dd error: ' + JSON.stringify(err));
    	});
    
    },error : function(xhr, errorType, error) {
	    logger.e("yinyien:" + _config.corpId);
	    alert(errorType + ', ' + error);
    }

});
    