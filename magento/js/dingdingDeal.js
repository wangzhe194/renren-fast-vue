$.ajax({
    type: 'GET',
    url: baseUrl+"/interface/getConfig",
    contentType:"application/json;charset=UTF-8",
   xhrFields: {
   withCredentials: true
   },
   crossDomain: true,
    success: function(data){
       var _config=data;
        //phone
        
       dd.config({
			agentId : _config.agentid,
			corpId : _config.corpId,
			timeStamp : _config.timeStamp,
			nonceStr : _config.nonceStr,
			signature : _config.signature,
			jsApiList : [ 'runtime.info', 'biz.contact.choose',
					'device.notification.confirm', 'device.notification.alert',
					'device.notification.prompt', 'biz.ding.post',
					'biz.util.openLink']
		});
       dd.ready(function() {
    	    dd.biz.navigation.setTitle({
    	        title: 'loading',
    	        onSuccess: function(data) {
    	        },
    	        onFail: function(err) {
    	            log.e(JSON.stringify(err));
    	        }
    	    });
    		// alert('dd.ready rocks!');

    		dd.runtime.info({
    			onSuccess : function(info) {
    				logger.e('runtime info: ' + JSON.stringify(info));
    			},
    			onFail : function(err) {
    				logger.e('fail: ' + JSON.stringify(err));
    			}
    		});
    		dd.ui.pullToRefresh.enable({
    		    onSuccess: function() {
    		    },
    		    onFail: function() {
    		    }
    		})

    		dd.biz.navigation.setMenu({
    			backgroundColor : "#ADD8E6",
    			items : [
    				{
    					id:"此处可以设置帮助",//字符串
    				// "iconId":"file",//字符串，图标命名
    				  text:"帮助"
    				}
    				,
    				{
    					"id":"2",
    				"iconId":"photo",
    				  "text":"我们"
    				}
    				,
    				{
    					"id":"3",
    				"iconId":"file",
    				  "text":"你们"
    				}
    				,
    				{
    					"id":"4",
    				"iconId":"time",
    				  "text":"他们"
    				}
    			],
    			onSuccess: function(data) {
    				alert(JSON.stringify(data));

    			},
    			onFail: function(err) {
    				alert(JSON.stringify(err));
    			}
    		});


    		dd.runtime.permission.requestAuthCode({
    			corpId : _config.corpId,
    			onSuccess : function(info) {
    				//alert('authcode: ' + info.code);
    		        $.ajax({
    		            type: 'POST',
    		            url: baseUrl+"/interface/index",
    		           contentType:"application/json;charset=UTF-8",
    		           data:JSON.stringify({'code':info.code,'corpid':_config.corpId}),
    		           xhrFields: {
    		           withCredentials: true
    		           },
    		           crossDomain: true,
    		            success: function(data){
    		            	//免登成功 此处进行页面跳转
                            if(typeof(data)=='string'){
                                var obj=JSON.parse(data);
                                $.cookie("tokenId",data.tokenId);
                            }
                            else{
                                $.cookie("tokenId",data.tokenId);    
                            }  
    		               window.location.href='index.html';
    		            },error : function(xhr, errorType, error) {
    					    logger.e("yinyien:" + _config.corpId);
    					    alert(errorType + ', ' + error);
    				    }
    		    
    		       });
    				
    			},
    			onFail : function(err) {
    				alert('fail: ' + JSON.stringify(err));
    			}
    		});
    		
    	});

    	dd.error(function(err) {
    		alert('dd error: ' + JSON.stringify(err));
    	});
        
        //pc
        
        
               DingTalkPC.config({
    agentId : _config.agentid,
    corpId : _config.corpId,
    timeStamp : _config.timeStamp,
    nonceStr : _config.nonceStr,
    signature : _config.signature,
    jsApiList : [ 'runtime.info', 'biz.contact.choose',
        'device.notification.confirm', 'device.notification.alert',
        'device.notification.prompt', 'biz.ding.post',
        'biz.util.openLink' ]
});
       DingTalkPC.ready(function() {
    DingTalkPC.biz.navigation.setTitle({
        title: 'loading',
        onSuccess: function(data) {
        },
        onFail: function(err) {
            log.e(JSON.stringify(err));
        }
    });

    DingTalkPC.runtime.info({
        onSuccess : function(info) {
            logger.e('runtime info: ' + JSON.stringify(info));
        },
        onFail : function(err) {
            logger.e('fail: ' + JSON.stringify(err));
        }
    });
    DingTalkPC.ui.pullToRefresh.enable({
        onSuccess: function() {
        },
        onFail: function() {
        }
    })
 DingTalkPC.runtime.permission.requestAuthCode({
        corpId : _config.corpId,
        onSuccess : function(info) {
			alert('authcode: ' + info.code);
             $.ajax({
    		            type: 'POST',
    		            url: baseUrl+"/interface/index",
    		           contentType:"application/json;charset=UTF-8",
    		           data:JSON.stringify({'code':info.code,'corpid':_config.corpId}),
    		           xhrFields: {
    		           withCredentials: true
    		           },
    		           crossDomain: true,
    		            success: function(data){
    		            	//免登成功 此处进行页面跳转
                            if(typeof(data)=='string'){
                                var obj=JSON.parse(data);
                                $.cookie("tokenId",data.data.tokenId);
                            }
                            else{
                                $.cookie("tokenId",data.data.tokenId);    
                            }  
    		               window.location.href='index.html';
    		            },error : function(xhr, errorType, error) {
    					    logger.e("yinyien:" + _config.corpId);
    					    alert(errorType + ', ' + error);
    				    }
    		    
    		       });

        },
        onFail : function(err) {
            alert('fail: ' + JSON.stringify(err));
        }
    });
});
           
    DingTalkPC.error(function(err) {
    alert('dd error: ' + JSON.stringify(err));
});
        
        
        
        
    
    },error : function(xhr, errorType, error) {
	    logger.e("yinyien:" + _config.corpId);
	    alert(errorType + ', ' + error);
    }

});